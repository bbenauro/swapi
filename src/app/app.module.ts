import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';


import { ButtonsModule, WavesModule, CardsFreeModule, CardsModule, ProgressbarModule, MDBBootstrapModulePro, MDBBootstrapModule } from 'ng-uikit-pro-standard'


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CharacterSummaryComponent } from './modules/Characters/components/character-summary/character-summary.component';
import { CharacterDetailComponent } from './modules/Characters/components/character-detail/character-detail.component';
import { CharacterListPageComponent } from './modules/Characters/pages/character-list-page/character-list-page.component';
import { ComponentLoaderFactory } from 'ng-uikit-pro-standard/lib/free/utils/component-loader/component-loader.factory';

@NgModule({
  declarations: [
    AppComponent,
    CharacterSummaryComponent,
    CharacterDetailComponent,
    CharacterListPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ButtonsModule,
    WavesModule,
    CardsFreeModule,
    CardsModule,
    ProgressbarModule,
    MDBBootstrapModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
