import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CharacterListPageComponent } from './modules/Characters/pages/character-list-page/character-list-page.component';

const routes: Routes = [
  { path: "characters", component: CharacterListPageComponent},
  { path: "", component: CharacterListPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 
}
