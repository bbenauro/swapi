import { Component, OnInit, Input } from '@angular/core';
import { CharacterAPIHelperServiceService } from '../../services/character-apihelper-service.service';

@Component({
  selector: 'app-character-detail',
  templateUrl: './character-detail.component.html',
  styleUrls: ['./character-detail.component.sass']
})
export class CharacterDetailComponent implements OnInit {
  @Input() character: any;

  movies: any[];
  keys: any;
  constructor(private swapi: CharacterAPIHelperServiceService) {
    this.keys = new Array<string>()
  }
  ngOnInit() {
  }
  
  ngOnChanges(changes: any): void {
    console.debug("Model Changed");
    this.keys = Object.keys(this.character);

    //Get Movies
    this.swapi.getMovieDetails(this.character.films).subscribe(x => {
      this.movies = x;
    },
      err => {
        console.log(err)
      }
    );
  }
}
