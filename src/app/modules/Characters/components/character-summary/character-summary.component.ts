import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-character-summary',
  templateUrl: './character-summary.component.html',
  styleUrls: ['./character-summary.component.sass']
})
export class CharacterSummaryComponent implements OnInit {


  @Input() character: any;
  @Output() displayCharacter = new EventEmitter<any>();


  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    console.log(JSON.stringify(this.character));
  }

  public doDisplay() {
    this.displayCharacter.emit(this.character);
  }
}
