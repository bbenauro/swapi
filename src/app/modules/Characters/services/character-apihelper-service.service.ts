import { Injectable } from '@angular/core';
import { Observable, forkJoin } from 'rxjs';
import { HttpClientModule, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CharacterAPIHelperServiceService {

  static rootUrl = "https://swapi.co/api/people";

  constructor(private httpClient: HttpClient) { }

  fetchCharacters(): Observable<any> {
    return this.httpClient.get(CharacterAPIHelperServiceService.rootUrl);
  }

  getMovieDetails(movies: String[]): Observable<any[]> {
    var moviesRequest = movies.map((x: string) => {
      return this.httpClient.get(x);
    })
    return forkJoin(moviesRequest);
  }
}
