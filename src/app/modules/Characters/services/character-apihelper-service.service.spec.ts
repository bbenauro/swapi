import { TestBed } from '@angular/core/testing';

import { CharacterAPIHelperServiceService } from './character-apihelper-service.service';

describe('CharacterAPIHelperServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CharacterAPIHelperServiceService = TestBed.get(CharacterAPIHelperServiceService);
    expect(service).toBeTruthy();
  });
});
