import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from 'selenium-webdriver/http';
import { CharacterAPIHelperServiceService } from '../../services/character-apihelper-service.service';


@Component({
  selector: 'app-character-list-page',
  templateUrl: './character-list-page.component.html',
  styleUrls: ['./character-list-page.component.sass']
})
export class CharacterListPageComponent implements OnInit {
  @ViewChild('characterDetailModal') public characterDetailModal;
  selectedCharacter: any = null;
  characters: any = [];
  constructor(private characterService: CharacterAPIHelperServiceService) { }

  ngOnInit() {

    this.characterService.fetchCharacters().subscribe(x => {
      this.characters = x.results
    },
    err => {
      console.log(err)
    });
  }

  displaySelectedCharacter(event) {
    this.selectedCharacter = event;
    this.characterDetailModal.show();
  }
}
