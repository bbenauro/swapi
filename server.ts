import { Router, Request, Response } from 'express';

const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const app = express();
//const dbConn = require('./dist/server/modules/dbConnection');
const http_port = process.env.PORT || '3000';

// Parsers
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Angular DIST output folder
app.use(express.static(path.join(__dirname, './SWAPI')));

app.get('/main.js', (req: Request, res: Response) => {
    res.sendFile(path.join(__dirname, './SWAPI/main.js'));
});

app.get('/polyfills.js', (req: Request, res: Response) => {
    res.sendFile(path.join(__dirname, './SWAPI/polyfills.js'));
});

app.get('/runtime.js', (req: Request, res: Response) => {
    res.sendFile(path.join(__dirname, './SWAPI/runtime.js'));
});

app.get('/styles.js', (req: Request, res: Response) => {
    res.sendFile(path.join(__dirname, './SWAPI/styles.js'));
});

app.get('/vendor.js', (req: Request, res: Response) => {
    res.sendFile(path.join(__dirname, './SWAPI/vendor.js'));
});

// Send all other requests to the Angular app
app.get('*', (req: Request, res: Response) => {
    res.sendFile(path.join(__dirname, './SWAPI/index.html'));
});

//Set Port
app.set('port', http_port);
const server = http.createServer(app);
server.listen(http_port, "0.0.0.0", () => console.log(`Running on localhost:${http_port}`));


